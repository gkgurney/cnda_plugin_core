package org.nrg.xnat.cndacore;

import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "cnda_plugin_core", name = "XNAT 1.7 Core Plugin", description = "This is the XNAT 1.7 Core Plugin.")
public class CorePlugin {
}
